import image from './assets/images/48.jpg'
import './App.css'
import Test from './Test';
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <div className='container devcamp-container'>
      <img src = {image} className = 'devcamp-avatar' />
      <p className='devcamp-quote'>This is one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p className='devcamp-intro'>      
        <span className='devcamp-name'>Tammy Stevens</span> - Front End Developer
      </p>
      <Test />
    </div>
  );
}

export default App;
